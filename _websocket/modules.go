package _websocket

type user struct {
	ID       string
	FullName string
	Photo    string
	Status   bool
}

type typeMessage struct {
	Type      string `json:"type"`
	Message   string `json:"message"`
	Recipient string `json:"recipient_id"`
	Sender    string `json:"sender"`
}

type typeGetAllMessages struct {
	Type       string `json:"type"`
	ReceiverID string `json:"receiver_id"`
	AdID       string `json:"ad_id"`
	Page       uint64 `json:"page"`
	Limit      uint64 `json:"limit"`
}
type typeGetMessagesOfSender struct {
	Type  string `json:"type"`
	Page  uint64 `json:"page"`
	Limit uint64 `json:"limit"`
}

type typeDeleteMessage struct {
	Type      string `json:"type"`
	MessageID string `json:"message_id"`
}

type typeUpdateMessage struct {
	Type      string `json:"type"`
	MessageID string `json:"message_id"`
	Message   string `json:"message"`
}

type reqMessage struct {
	Type    string `json:"type"`
	Message []byte
}

type resMessage struct {
	Message []byte
}

type typeRequest struct {
	Type string `json:"type"`
}
