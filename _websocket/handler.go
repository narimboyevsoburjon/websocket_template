package _websocket

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func WebSocketRun() {
	r := gin.Default()
	hub := NewHub()
	go hub.run()
	r.GET("/", func(c *gin.Context) {
		http.ServeFile(c.Writer, c.Request, "cmd/Index.html")
	})
	r.GET("/ws", func(c *gin.Context) {
		name := c.Query("username")
		fmt.Println(name, "1111")
		conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
		if err != nil {
			log.Println(err)
			return
		}
		ServeWs(hub, conn)
	})
	r.Run(":8080") // listen and serve on 0.0.0.0:8080

}
