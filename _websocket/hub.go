package _websocket

import (
	"encoding/json"
	"sync"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[string]*Client

	// Inbound messages from the clients.
	broadcast chan reqMessage

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
}

func NewHub() *Hub {
	return &Hub{
		broadcast:  make(chan reqMessage, 256),
		register:   make(chan *Client, 256),
		unregister: make(chan *Client, 256),
		clients:    make(map[string]*Client),
	}
}

func (h *Hub) run() {
	var wg sync.WaitGroup
	wg.Add(3)
	go func() {
		defer wg.Done()
		for {
			select {
			case client := <-h.register:
				h.clients[client.ID] = client
				h.userStatus(client.ID, true)
				h.messagesInMemory(client.ID)
				for i := 0; i < len(h.register); i++ {
					client := <-h.register
					h.clients[client.ID] = client
					h.userStatus(client.ID, true)
					h.messagesInMemory(client.ID)

				}
			}
		}
	}()
	go func() {
		wg.Done()
		for {
			select {
			case client := <-h.unregister:
				delete(h.clients, client.ID)
				close(client.send)
				h.userStatus(client.user.ID, false)
				for i := 0; i < len(h.register); i++ {
					client := <-h.register
					delete(h.clients, client.ID)
					close(client.send)
					h.clients[client.ID] = client
					h.userStatus(client.ID, false)

				}
			}
		}
	}()
	go func() {
		wg.Done()
		for {
			select {
			case Message := <-h.broadcast:
				h.filterMessage(Message)
				for i := 0; i < len(h.register); i++ {
					Message := <-h.broadcast
					h.filterMessage(Message)
				}
			}
		}

	}()
	wg.Wait()
}

func (h *Hub) messagesInMemory(id string) error {
	return nil
}

func (h *Hub) userStatus(id string, status bool) error {
	return nil
}

func (h *Hub) filterMessage(message reqMessage) {
	if message.Type == "message" {
		h.editMessage(message)
	} else if message.Type == "get_all_messages" {
		h.editGetAllMessages(message)
	} else if message.Type == "get_messages_of_sender" {
		h.editGetMessagesOfSender(message)
	} else if message.Type == "delete_message" {
		h.editDeleteMessage(message)
	} else if message.Type == "update_message" {
		h.editUpdateMessage(message)
	}
}

func (h *Hub) editMessage(m reqMessage) error {
	var message = typeMessage{}
	if err := json.Unmarshal(m.Message, &message); err != nil {
		return err
	}
	clent, ok := h.clients[message.Sender]
	if ok {
		var res = resMessage{}
		message := struct {
			Type    string `json:"type"`
			Message string `json:"message"`
			Sender  string `json:"sender"`
		}{}
		res.Message, _ = json.Marshal(message)
		clent.send <- res
	} else {
		//saveMessage
	}
	return nil
}

func (h *Hub) editGetAllMessages(m reqMessage) error {
	return nil
}

func (h *Hub) editGetMessagesOfSender(m reqMessage) error {
	return nil
}

func (h *Hub) editDeleteMessage(m reqMessage) error {
	return nil
}

func (h *Hub) editUpdateMessage(m reqMessage) error {
	return nil
}
