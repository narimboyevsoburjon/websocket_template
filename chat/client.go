package chat

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"log"
	"time"

	"github.com/gorilla/websocket"
)

type ReqMessage struct {
	Message   string `json:"message"`
	Recipient string `json:"recipient_id"`
	Sender    string `json:"sender"`
}

type ResMessage struct {
	Message string `json:"message"`
	Sender  string `json:"sender"`
}

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

type User struct {
	ID       string
	UserName string
	EnterAt  time.Time
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan ResMessage
	User
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		var message = ReqMessage{}
		err := c.conn.ReadJSON(&message)
		message.Sender = c.UserName
		fmt.Println(message)
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		//message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		//data := map[string][]byte{
		//	"message": message,
		//	"id":      []byte(c.ID),
		//}
		//userMessage, _ := json.Marshal(data)
		c.hub.broadcast <- message
	}
}

func (c *Client) writeMessages(messages []ReqMessage) {
	for _, v := range messages {
		c.conn.SetWriteDeadline(time.Now().Add(writeWait))

		w, err := c.conn.NextWriter(websocket.TextMessage)
		if err != nil {
			return
		}
		message := ResMessage{Message: v.Message, Sender: v.Sender}
		m, _ := json.Marshal(&message)
		w.Write(m)
		// Add queued chat messages to the current websocket message.

		if err := w.Close(); err != nil {
			return
		}
	}

}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			m, _ := json.Marshal(message)
			w.Write(m)

			// Add queued chat messages to the current websocket message.
			//n := len(c.send)
			//for i := 0; i < n; i++ {
			//	w.Write(<-c.send)
			//}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// ServeWs handles websocket requests from the peer.
func ServeWs(hub *Hub, conn *websocket.Conn, name string) {
	client := &Client{hub: hub, conn: conn, send: make(chan ResMessage)}
	client.hub.register <- client
	client.UserName = name
	client.ID = GenUserId()
	client.EnterAt = time.Now()

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}

func GenUserId() string {
	uid := uuid.NewString()
	return uid
}
