package chat

import (
	"fmt"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan ReqMessage

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	userMessage map[string][]ReqMessage
}

func NewHub() *Hub {
	return &Hub{
		broadcast:   make(chan ReqMessage),
		register:    make(chan *Client),
		unregister:  make(chan *Client),
		clients:     make(map[*Client]bool),
		userMessage: make(map[string][]ReqMessage),
	}
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			name := client.UserName
			for client := range h.clients {
				msg := ResMessage{
					Message: "on line",
					Sender:  name,
				}
				client.send <- msg
			}

			h.clients[client] = true
			massages, ok := h.userMessage[client.UserName]
			if ok {
				if len(massages) != 0 {
					client.writeMessages(massages)
				}
			}
			h.userMessage[client.UserName] = make([]ReqMessage, 0)

		case client := <-h.unregister:
			name := client.UserName
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
			for client := range h.clients {
				msg := ResMessage{
					Message: "of line",
					Sender:  name,
				}
				client.send <- msg
			}
		case userMessage := <-h.broadcast:
			//var data map[string][]byte
			//json.Unmarshal(userMessage, &data)
			//u := strings.Split(string(data["message"]), " ")[0]
			//user := u[:len(u)-1]
			//m := strings.Split(string(data["message"]), " ")
			//message := strings.Split(string(data["message"]), " ")[1]
			fmt.Println(userMessage)
			//for i := 2; i < len(m); i++ {
			//	message += m[2]
			//}
			s := 0
			for client := range h.clients {
				//prevent self receive the message
				if client.UserName == userMessage.Recipient {
					select {
					case client.send <- ResMessage{
						Message: userMessage.Message,
						Sender:  userMessage.Sender,
					}:
					default:
						close(client.send)
						delete(h.clients, client)
					}
					s++
					break
				}
			}
			if s == 0 {
				_, ok := h.userMessage[userMessage.Recipient]
				if !ok {
					h.userMessage[userMessage.Recipient] = make([]ReqMessage, 0)
				}
				h.userMessage[userMessage.Recipient] = append(h.userMessage[userMessage.Recipient], userMessage)
				fmt.Println(h.userMessage[userMessage.Recipient])
			}
		}
	}
}
