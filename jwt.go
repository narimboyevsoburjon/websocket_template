package websocket_template

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"strings"
)

type TokenMetadata struct {
	UserID      string
	Credentials map[string]string
	Expires     int64
}

func ExtractTokenMetadata(c *gin.Context) (*TokenMetadata, error) {
	token, err := verifyToken(c)
	if err != nil {
		return nil, err
	}

	// Setting and checking token and credentials.
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		// User ID.
		userID, err := uuid.Parse(claims["id"].(string))
		if err != nil {
			return nil, err
		}

		// Expires time.
		expires := claims["expires"].(float64)

		// User credentials.
		credentials := map[string]string{
			"role": claims["role"].(string),
		}

		return &TokenMetadata{
			UserID:      userID.String(),
			Credentials: credentials,
			Expires:     int64(expires),
		}, nil
	}

	return nil, err
}

func verifyToken(c *gin.Context) (*jwt.Token, error) {
	tokenString := extractToken(c)
	token, err := jwt.Parse(tokenString, jwtKeyFunc)
	if err != nil {
		return nil, err
	}

	return token, nil
}

func extractToken(c *gin.Context) string {
	bearToken := c.Query("Authorization")

	onlyToken := strings.Split(bearToken, " ")
	if len(onlyToken) == 1 {
		return onlyToken[0]
	}

	return ""
}

func jwtKeyFunc(token *jwt.Token) (interface{}, error) {
	return []byte(""), nil
}
